package com.dbthor.external.dtecloud.service.cesion;

import com.dbthor.domain.cliente.certificado.entity.CertificadoDigital;
import com.dbthor.domain.cliente.certificado.service.CertificadoService;
import com.dbthor.domain.cliente.documento.entity.aec.EAecData;
import com.dbthor.domain.cliente.documento.entity.dte.EDteData;
import com.dbthor.domain.cliente.documento.service.DteClientService;
import com.dbthor.domain.cliente.persona.entity.Identificacion;
import com.dbthor.domain.cliente.persona.service.PersonaService;
import com.dbthor.exception.ServiceException;
import com.dbthor.exception.ServiceExceptionCodes;
import com.dbthor.external.dtecloud.entity.request.cesion.Cesiones;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Clase servicio para manejar el idRespuesta
 *
 * @author CMIRANDA
 * @version 1.0
 */
@SuppressWarnings({"unchecked", "WeakerAccess"})
@Service
@Log4j2
public class CesionesService {

    @Autowired
    PersonaService persSrv;
    @Autowired
    CertificadoService certSrv;
    @Autowired
    DteClientService dteSrv;




    public String cesion(Cesiones cesiones, UUID trxId) throws ServiceException {
        Identificacion cedente = null, cedenteUsuario = null, cesionario = null;
        try {

            log.debug("{} Validando Cedente {}", trxId, cesiones.getCedenteId());
            if (cesiones.getCedenteId() != null) {
                cedente = persSrv.getIdentificacion(cesiones.getCedenteId(), 1, trxId);
                if (cedente == null)
                    throw new ServiceException(ServiceExceptionCodes.PERSONA_NO_EXISTE, "Cedente");
            } else {
                throw new ServiceException(ServiceExceptionCodes.PERSONA_NO_EXISTE, "Cedente");
            }
            log.debug("{} Validando CedenteUsuario {}", trxId, cesiones.getCedenteUsuarioId());
            if (cesiones.getCedenteUsuarioId() != null) {
                cedenteUsuario = persSrv.getIdentificacion(cesiones.getCedenteUsuarioId(), 1, trxId);
                if (cedenteUsuario == null)
                    throw new ServiceException(ServiceExceptionCodes.PERSONA_NO_EXISTE, "CedenteUsuario");
            } else {
                throw new ServiceException(ServiceExceptionCodes.PERSONA_NO_EXISTE, "CedenteUsuario");
            }
            log.debug("{} Validando Cecionario {}", trxId, cesiones.getCecionarioId());
            if (cesiones.getCecionarioId() != null) {
                cesionario = persSrv.getIdentificacion(cesiones.getCecionarioId(), 1, trxId);
                if (cesionario == null)
                    throw new ServiceException(ServiceExceptionCodes.PERSONA_NO_EXISTE, "Cesionario");
            } else {
                throw new ServiceException(ServiceExceptionCodes.PERSONA_NO_EXISTE, "Cesionario");
            }
            log.debug("{} Validando Certificado {}", trxId, cesiones.getCertificadoId());
            CertificadoDigital cert = certSrv.getCertificado(cesiones.getCertificadoId(), cesiones.getClave(), trxId);
            if (cert == null) {
                throw new ServiceException(ServiceExceptionCodes.CERTIFICADO_NO_EXISTE);
            }

            //AGREGAR LOGIN


            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


            //get lista de DTEData & zip
            List<EDteData> listDteData = new ArrayList<>();
            if (cesiones.getListDte() != null) {
                for (UUID d : cesiones.getListDte()) {
                    listDteData.add(dteSrv.getDteData(d, trxId));
                }
            }
            //get lista de AecData & zip
            List<EAecData> listAecData = new ArrayList<>();
            if (cesiones.getListAec() != null) {
                for (UUID d : cesiones.getListAec()) {
                    listAecData.add(dteSrv.getAecData(d, trxId));
                }
            }


            log.debug("{} END", trxId);
            return "un id";

        } catch (Exception e) {
            ServiceException fbe = (e instanceof ServiceException ? (ServiceException) e : new ServiceException(ServiceExceptionCodes.ERROR_NO_ADMINISTRADO, e));
            log.error("{} {}", trxId, fbe.getErrorLog());
            log.debug("{} END ERR", trxId);
            throw fbe;
        }
    }

}

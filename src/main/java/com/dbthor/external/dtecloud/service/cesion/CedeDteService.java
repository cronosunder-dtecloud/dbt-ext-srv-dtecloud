package com.dbthor.external.dtecloud.service.cesion;

import com.dbthor.domain.cliente.certificado.service.CertificadoService;
import com.dbthor.domain.cliente.documento.service.DteClientService;
import com.dbthor.domain.cliente.persona.service.PersonaService;
import com.dbthor.exception.ServiceException;
import com.dbthor.exception.ServiceExceptionCodes;
import com.dbthor.external.dtecloud.entity.request.cesion.CederDteRequest;
import com.dbthor.external.dtecloud.entity.response.cesion.CedeDteResponse;
import com.dbthor.tools.SOAPUtils;
import com.dbthor.tools.services.soap.SoapCall;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import javax.xml.soap.SOAPMessage;
import java.util.UUID;

/**
 * Clase servicio para manejar el idRespuesta
 *
 * @author CMIRANDA
 * @version 1.0
 */
@SuppressWarnings({"unchecked", "WeakerAccess"})
@Service
@Log4j2
public class CedeDteService {

    @Value("${url.srv.cesione.path}")
    private String endPointService;
    @Value("${url.srv.cesione.ceder.documento}")
    private String urlCederDocumento;
    @Value("${cesion.enveloped.ceder.documento}")
    private String cederDocumentoEnveloped;

    public CedeDteResponse cederDte(CederDteRequest request, UUID trxId) throws ServiceException {
        try {
            String endPoint = endPointService;
            log.debug("{} START", trxId);
            log.debug("{} Token {}", trxId, request.getToken());

            CedeDteResponse response = new CedeDteResponse(trxId);

            String envelope = cederDocumentoEnveloped;
            envelope = envelope.replace("{TOKEN}", request.getToken());
            envelope = envelope.replace("{FOLIO}", String.valueOf(request.getFolio()));
            envelope = envelope.replace("{TIPODTE}", String.valueOf(request.getTipoDte()));
            envelope = envelope.replace("{RUTEMISOR}", request.getRutEmisor());
            envelope = envelope.replace("{IDSECUENCIA}", String.valueOf(request.getIdSecuencia()));
            envelope = envelope.replace("{RUTCESIONARIO}", request.getRutCesionario());
            envelope = envelope.replace("{RAZCESIONARIO}", request.getRazCesionario());
            envelope = envelope.replace("{DIRCESIONARIO}", request.getDirCesionario());
            envelope = envelope.replace("{EMAILCESIOARIO}", request.getEmailCesionario());
            envelope = envelope.replace("{NOMBREEJECUTIVO}", request.getNombreEjecutivo());
            envelope = envelope.replace("{EMAILEJECUTIVO}", request.getEmailEjecutivo());
            envelope = envelope.replace("{RUTAUTORIZADOSII}", request.getRutAutorizadoSii());
            envelope = envelope.replace("{NOMBREAUTORIZADOSII}", request.getNombreAutorizadoSii());
            envelope = envelope.replace("{FECHAVENCIMIENTO}", request.getFechaVencimiento());
            envelope = envelope.replace("{EMAILDEUDOR}", request.getEmailDeudor());

            SoapCall soap = new SoapCall(endPoint);
            soap.setMessage(SOAPUtils.obtenerSoap(envelope));
            soap.addHeader("SOAPAction", urlCederDocumento);
            SOAPMessage respSoap = soap.call(trxId);

            Document resp = SOAPUtils.soapToDocument(respSoap);
            if (resp != null && SOAPUtils.consultar("//return", resp) != null) {
                String x = SOAPUtils.consultar("//return/item[1]/DescripcionResultado", resp);
                if (x != null && !x.equalsIgnoreCase("NULL")) {
                    response.asignarCodigo(response.codes().valueOf("E_" + x));
                } else {
                    response.asignarRespuesta(CedeDteResponse.RESPUESTAS.ERR_2);

                    x = SOAPUtils.consultar("//return/item[1]/ResultadoFE", resp);
                    response.setDescripcionRespuesta(x);
                }
            }
            if (response.getActCode() == response.codes().E_TVAL || response.getActCode() == response.codes().E_TDUP) {
                String x = SOAPUtils.consultar("//return/item[1]/IdResultadoFE", resp);
                if (!x.equalsIgnoreCase("NULL")) {
                    response.asignarRespuesta(response.respuestas().valueOf("E_" + x));

                } else {
                    response.asignarRespuesta(CedeDteResponse.RESPUESTAS.ERR_2);

                    x = SOAPUtils.consultar("//return/item[1]/ResultadoFE", resp);
                    response.setDescripcionRespuesta(x);
                }
            }
            log.debug("{} END", trxId);
            return response;

        } catch (Exception e) {
            ServiceException fbe = (e instanceof ServiceException ? (ServiceException) e : new ServiceException(ServiceExceptionCodes.ERROR_NO_ADMINISTRADO, e));
            log.error("{} {}", trxId, fbe.getErrorLog());
            log.debug("{} END ERR", trxId);
            throw fbe;
        }
    }
}

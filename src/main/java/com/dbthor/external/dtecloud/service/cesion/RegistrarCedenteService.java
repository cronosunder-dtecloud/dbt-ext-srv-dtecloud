package com.dbthor.external.dtecloud.service.cesion;

import com.dbthor.domain.cliente.certificado.entity.CertificadoDigital;
import com.dbthor.domain.cliente.certificado.service.CertificadoService;
import com.dbthor.domain.cliente.documento.entity.aec.EAecData;
import com.dbthor.domain.cliente.documento.entity.dte.EDteData;
import com.dbthor.domain.cliente.documento.service.DteClientService;
import com.dbthor.domain.cliente.persona.entity.Identificacion;
import com.dbthor.domain.cliente.persona.service.PersonaService;
import com.dbthor.exception.ServiceException;
import com.dbthor.exception.ServiceExceptionCodes;
import com.dbthor.external.dtecloud.entity.request.cesion.Cesiones;
import com.dbthor.external.dtecloud.entity.request.registra.cedente.RegistraCedenteRequest;
import com.dbthor.external.dtecloud.entity.response.registra.cedente.RegistraCedenteResponse;
import com.dbthor.tools.SOAPUtils;
import com.dbthor.tools.services.soap.SoapCall;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import javax.xml.soap.SOAPMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Clase servicio para manejar el idRespuesta
 *
 * @author CMIRANDA
 * @version 1.0
 */
@SuppressWarnings({"unchecked", "WeakerAccess"})
@Service
@Log4j2
public class RegistrarCedenteService {

    @Value("${url.srv.cesione.path}")
    private String endPointService;
    @Value("${url.srv.cesione.registra.cedente}")
    private String urlRegistroCedente;
    @Value("${cesion.enveloped.registra.cedente}")
    private String registraCedenteEnveloped;


    public RegistraCedenteResponse registraCedente(RegistraCedenteRequest cedente, UUID trxId) throws ServiceException {
        try {
            String endPoint = endPointService;
            log.debug("{} START", trxId);
            log.debug("{} getNombreAutorizado {}", trxId, cedente.getNombreAutorizado());
            log.debug("{} getRazonSocial {}", trxId, cedente.getRazonSocial());
            log.debug("{} getRutAutorizado {}", trxId, cedente.getRutAutorizado());
            log.debug("{} getRutEmpresa {}", trxId, cedente.getRutEmpresa());
            log.debug("{} getRutIntegrador {}", trxId, cedente.getRutIntegrador());

            RegistraCedenteResponse response = new RegistraCedenteResponse(trxId);

            String envelope = registraCedenteEnveloped;
            envelope = envelope.replace("{RUT}", cedente.getRutEmpresa());
            envelope = envelope.replace("{RAZONSOCIAL}", cedente.getRazonSocial());
            envelope = envelope.replace("{RUTAUTORIZADOSII}", cedente.getRutAutorizado());
            envelope = envelope.replace("{NOMBREAUTORIZADOSII}", cedente.getNombreAutorizado());
            envelope = envelope.replace("{CERTDIGBASE64}", cedente.getCertEncode());
            envelope = envelope.replace("{PASSCERTDIG}", cedente.getCertPass());
            envelope = envelope.replace("{RUTINTEGRADOR}", cedente.getRutIntegrador() != null ? cedente.getRutIntegrador() : "");

            SoapCall soap = new SoapCall(endPoint);
            soap.setMessage(SOAPUtils.obtenerSoap(envelope));
            soap.addHeader("SOAPAction", urlRegistroCedente);
            SOAPMessage respSoap = soap.call(trxId);

            Document resp = SOAPUtils.soapToDocument(respSoap);
            if (resp != null && SOAPUtils.consultar("//return", resp) != null) {
                String x = SOAPUtils.consultar("//return/item[1]/DescripcionResultado", resp);
                response.asignarCodigo(response.codes().valueOf("E_" + x));
            }
            if (response.getActCode() == response.codes().E_TVAL || response.getActCode() == response.codes().E_TDUP) {
                String x = SOAPUtils.consultar("//return/item[1]/IdResultadoFE", resp);
                response.asignarRespuesta(response.respuestas().valueOf("E_" + x));
                x = SOAPUtils.consultar("//return/item[1]/PassAccesoApi", resp);
                response.setPassAccesoApi(x);
            }
            log.debug("{} END", trxId);
            return response;

        } catch (Exception e) {
            ServiceException fbe = (e instanceof ServiceException ? (ServiceException) e : new ServiceException(ServiceExceptionCodes.ERROR_NO_ADMINISTRADO, e));
            log.error("{} {}", trxId, fbe.getErrorLog());
            log.debug("{} END ERR", trxId);
            throw fbe;
        }
    }

}

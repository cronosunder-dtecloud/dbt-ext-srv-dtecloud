package com.dbthor.external.dtecloud.service.cesion;

import com.dbthor.exception.ServiceException;
import com.dbthor.exception.ServiceExceptionCodes;
import com.dbthor.external.dtecloud.entity.request.cesion.ConsultaEstadoAECRequest;
import com.dbthor.external.dtecloud.entity.response.cesion.ConsultaEstadoAECResponse;
import com.dbthor.tools.SOAPUtils;
import com.dbthor.tools.services.soap.SoapCall;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import javax.xml.soap.SOAPMessage;
import java.util.UUID;

/**
 * Clase servicio para manejar el idRespuesta
 *
 * @author CMIRANDA
 * @version 1.0
 */
@SuppressWarnings({"unchecked", "WeakerAccess"})
@Service
@Log4j2
public class ConsultaEstadoAECService {

    @Value("${url.srv.cesione.path}")
    private String endPointService;
    @Value("${url.srv.cesione.consultar.documento.aec}")
    private String urlConsultarDocumentoAec;
    @Value("${cesion.enveloped.consulta.documento.aec}")
    private String consultarDocumentoAECEnveloped;

    public ConsultaEstadoAECResponse ConsultaEstadoAEC(ConsultaEstadoAECRequest request, UUID trxId) throws ServiceException {
        try {
            String endPoint = endPointService;
            log.debug("{} START", trxId);
            log.debug("{} Token {}", trxId, request.getToken());

            ConsultaEstadoAECResponse response = new ConsultaEstadoAECResponse(trxId);

            String envelope = consultarDocumentoAECEnveloped;
            envelope = envelope.replace("{TOKEN}", request.getToken());
            envelope = envelope.replace("{FOLIO}", String.valueOf(request.getFolio()));
            envelope = envelope.replace("{TIPODTE}", String.valueOf(request.getTipoDte()));
            envelope = envelope.replace("{RUTEMISOR}", request.getRutEmisor());
            envelope = envelope.replace("{IDSECUENCIA}", String.valueOf(request.getIdSecuencia()));

            SoapCall soap = new SoapCall(endPoint);
            soap.setMessage(SOAPUtils.obtenerSoap(envelope));
            soap.addHeader("SOAPAction", urlConsultarDocumentoAec);
            SOAPMessage respSoap = soap.call(trxId);

            Document resp = SOAPUtils.soapToDocument(respSoap);
            if (resp != null && SOAPUtils.consultar("//return", resp) != null) {
                String x = SOAPUtils.consultar("//return/item[1]/DescripcionResultado", resp);
                if (x != null && !x.equalsIgnoreCase("NULL")) {
                    response.asignarCodigo(response.codes().valueOf("E_" + x));
                } else {
                    response.asignarRespuesta(ConsultaEstadoAECResponse.RESPUESTAS.ERR_2);
                    x = SOAPUtils.consultar("//return/item[1]/ResultadoFE", resp);
                    response.setDescripcionRespuesta(x);
                }
            }
            //Preguntamos si nos responde con token valido
            if (response.getActCode() == response.codes().E_TVAL || response.getActCode() == response.codes().E_TDUP) {
                String x = SOAPUtils.consultar("//return/item[1]/IdResultadoFE", resp);
                if (!x.equalsIgnoreCase("NULL")) {
                    response.asignarRespuesta(response.respuestas().valueOf("E_" + x));
                } else {
                    response.asignarRespuesta(ConsultaEstadoAECResponse.RESPUESTAS.ERR_2);
                    x = SOAPUtils.consultar("//return/item[1]/ResultadoFE", resp);
                    response.setDescripcionRespuesta(x);
                }

                x = SOAPUtils.consultar("//return/item[1]/CodigoCesion", resp);
                if (!x.equalsIgnoreCase("NULL")) {
                    response.asignarCodigoCesion(response.codesCesion().valueOf("E_" + x));
                } else {
                    response.asignarRespuesta(ConsultaEstadoAECResponse.RESPUESTAS.ERR_2);
                    x = SOAPUtils.consultar("//return/item[1]/ResultadoFE", resp);
                    response.setDescripcionRespuesta(x);
                }

                x = SOAPUtils.consultar("//return/item[1]/UrlAec", resp);
                response.setUrlAec(x != null && !x.equalsIgnoreCase("NULL") ? x : null);
                x = SOAPUtils.consultar("//return/item[1]/TrackIDCesion", resp);
                response.setTrackIDCesion(x != null && !x.equalsIgnoreCase("NULL") ? x : null);

            }
            log.debug("{} END", trxId);
            return response;

        } catch (Exception e) {
            ServiceException fbe = (e instanceof ServiceException ? (ServiceException) e : new ServiceException(ServiceExceptionCodes.ERROR_NO_ADMINISTRADO, e));
            log.error("{} {}", trxId, fbe.getErrorLog());
            log.debug("{} END ERR", trxId);
            throw fbe;
        }
    }

}

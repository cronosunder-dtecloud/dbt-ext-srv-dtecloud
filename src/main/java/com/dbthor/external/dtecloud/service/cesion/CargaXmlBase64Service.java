package com.dbthor.external.dtecloud.service.cesion;

import com.dbthor.exception.ServiceException;
import com.dbthor.exception.ServiceExceptionCodes;
import com.dbthor.external.dtecloud.entity.request.cesion.CargarXmlBase64Request;
import com.dbthor.external.dtecloud.entity.response.cesion.CargarXmlBase64Response;
import com.dbthor.tools.SOAPUtils;
import com.dbthor.tools.services.soap.SoapCall;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import javax.xml.soap.SOAPMessage;
import java.util.UUID;

/**
 * Clase servicio para manejar el idRespuesta
 *
 * @author CMIRANDA
 * @version 1.0
 */
@SuppressWarnings({"unchecked", "WeakerAccess"})
@Service
@Log4j2
public class CargaXmlBase64Service {

    @Value("${url.srv.cesione.path}")
    private String endPointService;
    @Value("${url.srv.cesione.carga.xml.base64}")
    private String urlCargaXMLBase64;
    @Value("${cesion.enveloped.carga.xml.base64}")
    private String cargaXmlBase64Enveloped;


    public CargarXmlBase64Response cargaBase64(CargarXmlBase64Request request, UUID trxId) throws ServiceException {
        try {
            String endPoint = endPointService;
            log.debug("{} START", trxId);
            log.debug("{} XML {}", trxId, request.getBase64encode() != null);
            log.debug("{} Token {}", trxId, request.getToken());

            CargarXmlBase64Response response = new CargarXmlBase64Response(trxId);

            String envelope = cargaXmlBase64Enveloped;
            envelope = envelope.replace("{TOKEN}", request.getToken());
            envelope = envelope.replace("{XMLBASE64}", request.getBase64encode());

            SoapCall soap = new SoapCall(endPoint);
            soap.setMessage(SOAPUtils.obtenerSoap(envelope));
            soap.addHeader("SOAPAction", urlCargaXMLBase64);
            SOAPMessage respSoap = soap.call(trxId);

            Document resp = SOAPUtils.soapToDocument(respSoap);
            if (resp != null && SOAPUtils.consultar("//return", resp) != null) {
                String x = SOAPUtils.consultar("//return/item[1]/DescripcionResultado", resp);
                response.asignarCodigo(response.codes().valueOf("E_" + x));
            }
            if (response.getActCode() == response.codes().E_TVAL || response.getActCode() == response.codes().E_TDUP) {
                String x = SOAPUtils.consultar("//return/item[1]/IdResultadoFE", resp);
                response.asignarRespuesta(response.respuestas().valueOf("E_" + x));
                x = SOAPUtils.consultar("//return/item[1]/TxtCesion", resp);
                response.setTxtCesion(x);
            }
            log.debug("{} END", trxId);
            return response;

        } catch (Exception e) {
            ServiceException fbe = (e instanceof ServiceException ? (ServiceException) e : new ServiceException(ServiceExceptionCodes.ERROR_NO_ADMINISTRADO, e));
            log.error("{} {}", trxId, fbe.getErrorLog());
            log.debug("{} END ERR", trxId);
            throw fbe;
        }
    }


}

package com.dbthor.external.dtecloud.service.autentificacion;

import com.dbthor.external.dtecloud.entity.request.token.ObtenerTokenRequest;
import com.dbthor.external.dtecloud.entity.request.token.ValidarTokenRequest;
import com.dbthor.external.dtecloud.entity.response.token.ObtenerTokenResponse;
import com.dbthor.external.dtecloud.entity.response.token.ValidarTokenResponse;
import com.dbthor.tools.services.soap.SoapCall;
import com.dbthor.exception.ServiceException;
import com.dbthor.exception.ServiceExceptionCodes;
import com.dbthor.tools.SOAPUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPMessage;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.UUID;

/**
 * Clase servicio para manejar el idRespuesta
 *
 * @author CMIRANDA
 * @version 1.0
 */
@SuppressWarnings({"unchecked", "WeakerAccess"})
@Service
@Log4j2
public class TokenService {

    @Value("${url.srv.cesione.path}")
    private String endPointService;

    @Value("${url.srv.cesione.token}")
    private String urlToken;
    @Value("${url.srv.cesione.valida.token}")
    private String urlValidaToken;

    /**
     * Esquema Basico para envelope de la solicitud get idRespuesta
     */
    @Getter
    @Setter
    private String getTokenEnveloped = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ces=\"https://www.cesione.cl\">\n" +
            "   <soapenv:Header/>\n" +
            "   <soapenv:Body>\n" +
            "      <ces:ObtenerToken>\n" +
            "         <RUTACCESOAPI>{RUTACCESOAPI}</RUTACCESOAPI>\n" +
            "         <PASSACCESOAPI>{PASSACCESOAPI}</PASSACCESOAPI>\n" +
            "      </ces:ObtenerToken>\n" +
            "   </soapenv:Body>\n" +
            "</soapenv:Envelope>";
    /**
     * Esquema Basico el enveloped validaToken
     */
    @Getter
    @Setter
    public String validarTokenExtEnveloped = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ces=\"https://www.cesione.cl\">\n" +
            "   <soapenv:Header/>\n" +
            "   <soapenv:Body>\n" +
            "      <ces:ValidarTokenExt>\n" +
            "         <TOKEN>{TOKEN}</TOKEN>\n" +
            "      </ces:ValidarTokenExt>\n" +
            "   </soapenv:Body>\n" +
            "</soapenv:Envelope>";


    @SuppressWarnings("unchecked")
    public ObtenerTokenResponse getToken(ObtenerTokenRequest tokenRequest, UUID trxId)
            throws Exception {
        ObtenerTokenResponse response = new ObtenerTokenResponse(ObtenerTokenResponse.CODES.ERR_0, trxId);
        String endPoint = urlToken;

        log.debug("{} START", trxId);
        log.debug("{} PARAM tokenRequest:    {}", trxId, tokenRequest.toString());

        SoapCall soap = new SoapCall(endPoint);
        try {
            String envelope = getTokenEnveloped;
            envelope = envelope.replace("{RUTACCESOAPI}", tokenRequest.getRut());
            envelope = envelope.replace("{PASSACCESOAPI}", tokenRequest.getClave());

            soap.setMessage(SOAPUtils.obtenerSoap(envelope));
            //Asignamos el Token al header Siempre despues de asignar el mensaje
            soap.addHeader("SOAPAction", urlToken);
        } catch (Exception e) {
            throw new ServiceException(ServiceExceptionCodes.SERV_ERR_SOAP_MENSAJE, e);
        }
        SOAPMessage respSoap = soap.call(trxId);
        try {
            Document resp = SOAPUtils.soapToDocument(respSoap);
            if (resp != null && SOAPUtils.consultar("//return", resp) != null) {
                String x = SOAPUtils.consultar("//return/item[1]/DescripcionResultado", resp);
                response.asignarCodigo(ObtenerTokenResponse.CODES.valueOf("E_" + x));
            }
            if (response.getActCode() == ObtenerTokenResponse.CODES.E_TOK || response.getActCode() == ObtenerTokenResponse.CODES.E_TDUP) {
                String x = SOAPUtils.consultar("//return/item[1]/Token", resp);
                response.setToken(x);
            }

        } catch (XPathExpressionException | ParserConfigurationException | SAXException | IOException e) {
            throw new ServiceException(ServiceExceptionCodes.SERV_ERR_SOAP_MENSAJE_LEER, e);
        }
        log.debug("{} END", trxId);

        return response;
    }

    @SuppressWarnings("unchecked")
    public ValidarTokenResponse validarToken(ValidarTokenRequest tokenRequest, UUID trxId)
            throws Exception {
        log.debug("{} START", trxId);
        ValidarTokenResponse response = new ValidarTokenResponse(ValidarTokenResponse.CODES.ERR_0, trxId);
        String endPoint = urlToken;

        SoapCall soap = new SoapCall(endPoint);
        try {
            String envelope = validarTokenExtEnveloped;
            envelope.replace("{TOKEN}", tokenRequest.getToken());

            soap.setMessage(SOAPUtils.obtenerSoap(envelope));
            //Asignamos el Token al header Siempre despues de asignar el mensaje
            soap.addHeader("SOAPAction", urlValidaToken);
        } catch (Exception e) {
            throw new ServiceException(ServiceExceptionCodes.SERV_ERR_SOAP_MENSAJE, e);
        }

        SOAPMessage respSoap = soap.call(trxId);
        try {
            Document resp = SOAPUtils.soapToDocument(respSoap);
            if (resp != null && SOAPUtils.consultar("//return", resp) != null) {
                String x = SOAPUtils.consultar("//return/item[1]/DescripcionResultado", resp);
                response.asignarCodigo(ValidarTokenResponse.CODES.valueOf("E_" + x));
            }

        } catch (XPathExpressionException | ParserConfigurationException | SAXException | IOException e) {
            throw new ServiceException(ServiceExceptionCodes.SERV_ERR_SOAP_MENSAJE_LEER, e);
        }
        log.debug("{} END", trxId);

        return response;

    }

}

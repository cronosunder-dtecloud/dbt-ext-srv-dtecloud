/**
 * En este paquete se deben colocar las clases que se conectan a la base de datos, POJOS.
 *
 *
 * @since 1.0
 * @author CMIRANDA
 * @version 1.0
 */
package com.dbthor.external.dtecloud.data.entity;
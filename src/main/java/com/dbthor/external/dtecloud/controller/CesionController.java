package com.dbthor.external.dtecloud.controller;

import com.dbthor.exception.ServiceException;
import com.dbthor.external.dtecloud.entity.request.cesion.CargarXmlBase64Request;
import com.dbthor.external.dtecloud.entity.request.cesion.CederDteRequest;
import com.dbthor.external.dtecloud.entity.request.cesion.Cesiones;
import com.dbthor.external.dtecloud.entity.request.cesion.ConsultaEstadoAECRequest;
import com.dbthor.external.dtecloud.entity.request.registra.cedente.RegistraCedenteRequest;
import com.dbthor.external.dtecloud.entity.response.cesion.CargarXmlBase64Response;
import com.dbthor.external.dtecloud.entity.response.cesion.CedeDteResponse;
import com.dbthor.external.dtecloud.entity.response.cesion.ConsultaEstadoAECResponse;
import com.dbthor.external.dtecloud.entity.response.registra.cedente.RegistraCedenteResponse;
import com.dbthor.external.dtecloud.service.cesion.*;
import com.dbthor.tools.services.ServiceResponseType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Api de acceso a servicios de cesione para manejo detoken
 * <p>
 * Created by CMIRANDA on 25-08-2017.
 */
@Api
@RestController
@RequestMapping(CesionController.BASE_URI)
@Log4j2
@SuppressWarnings("unused,WeakerAccess")
public class CesionController {
    static final String BASE_URI = "/dtecloud/cesion";

    final CesionesService cesionesServiceService;
    final RegistrarCedenteService registrarCedenteService;
    final CargaXmlBase64Service cargaXmlBase64Service;
    final CedeDteService cedeDteService;
    final ConsultaEstadoAECService consultaEstadoAECService;

    //------------------------------------------------------------------------------------------------------------------
    @Autowired
    public CesionController(CesionesService cesionesServiceService, RegistrarCedenteService registrarCedenteService, CargaXmlBase64Service cargaXmlBase64Service, CedeDteService cedeDteService, ConsultaEstadoAECService consultaEstadoAECService) {
        this.cesionesServiceService = cesionesServiceService;
        this.registrarCedenteService = registrarCedenteService;
        this.cargaXmlBase64Service = cargaXmlBase64Service;
        this.cedeDteService = cedeDteService;
        this.consultaEstadoAECService = consultaEstadoAECService;
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * @param cesiones
     * @param trxId
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "Servicio recibe el usuario, cesionario y los documentos que se sederan"
            , notes = "Servicio realiza las acciones nesesarias para realizar la cesion de los datos enviados"
    )
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ServiceResponseType<String>> ceder(
            @ApiParam(value = "Cesiones a realizar") @RequestBody Cesiones cesiones,
            @ApiParam(value = "Transaccion Id (UUID)") @RequestParam(required = false) UUID trxId
    ) {
        if (trxId == null) trxId = UUID.randomUUID();
        ControllerLinkBuilder link = linkTo(methodOn(CesionController.class).ceder(cesiones, trxId));
        ServiceResponseType<String> resp = new ServiceResponseType<>(trxId);
        log.debug("{} ----------------------------------------------------------------------------", trxId);
        try {
            log.debug("{} START", trxId);
            log.debug("{} GET   {}", trxId, link.toUri().toString());
            resp.add(link.withSelfRel());

            // Comienso del proceso
            String response = cesionesServiceService.cesion(cesiones, trxId);

            resp.setDatos(response);
        } catch (Exception e) {
            ServiceException se = ServiceException.assignException(e);
            resp.addError(se);
            log.error("{} {}", trxId, ServiceResponseType.getErrorMsg(resp.getError()));
        }
        log.debug("{} END", trxId);
        log.debug("{} ----------------------------------------------------------------------------", trxId);
        return new ResponseEntity<>(resp, ServiceResponseType.getHttpStatus(resp.getError(), HttpStatus.CREATED));
    }

    @RequestMapping(value = "registrar", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "Metodo que recive un cedente y lo registra en el servicio de Abstrahere."
            , notes = "interfaz que registra un cedente en abstrahere"
    )
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ServiceResponseType<RegistraCedenteResponse>> registraCedente(
            @ApiParam(value = "Obtener idRespuesta request") @RequestBody RegistraCedenteRequest registraCedenteRequest,
            @ApiParam(value = "Transaccion Id (UUID)") @RequestParam(required = false) UUID trxId
    ) {
        if (trxId == null) trxId = UUID.randomUUID();
        ControllerLinkBuilder link = linkTo(methodOn(CesionController.class).registraCedente(registraCedenteRequest, trxId));
        ServiceResponseType<RegistraCedenteResponse> resp = new ServiceResponseType<>(trxId);
        log.debug("{} ----------------------------------------------------------------------------", trxId);
        try {
            log.debug("{} START", trxId);
            log.debug("{} GET   {}", trxId, link.toUri().toString());
            resp.add(link.withSelfRel());

            // Comienso del proceso
            RegistraCedenteResponse response = registrarCedenteService.registraCedente(registraCedenteRequest, trxId);

            resp.setDatos(response);
        } catch (Exception e) {
            ServiceException se = ServiceException.assignException(e);
            resp.addError(se);
            log.error("{} {}", trxId, ServiceResponseType.getErrorMsg(resp.getError()));
        }
        log.debug("{} END", trxId);
        log.debug("{} ----------------------------------------------------------------------------", trxId);
        return new ResponseEntity<>(resp, ServiceResponseType.getHttpStatus(resp.getError(), HttpStatus.CREATED));
    }


    @RequestMapping(value = "cargar/base64", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "Metodo al que se envia el base64 del documento a ceder"
            , notes = "insterfaz para consumo de servicio de carga de base64"
    )
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ServiceResponseType<CargarXmlBase64Response>> cederDte(
            @ApiParam(value = "Obtener idRespuesta request") @RequestBody CargarXmlBase64Request request,
            @ApiParam(value = "Transaccion Id (UUID)") @RequestParam(required = false) UUID trxId
    ) {
        if (trxId == null) trxId = UUID.randomUUID();
        ControllerLinkBuilder link = linkTo(methodOn(CesionController.class).cederDte(request, trxId));

        ServiceResponseType<CargarXmlBase64Response> resp = new ServiceResponseType<>(trxId);
        log.debug("{} ----------------------------------------------------------------------------", trxId);
        try {
            log.debug("{} START", trxId);
            log.debug("{} GET   {}", trxId, link.toUri().toString());
            resp.add(link.withSelfRel());

            // Comienso del proceso
            CargarXmlBase64Response response = cargaXmlBase64Service.cargaBase64(request, trxId);

            resp.setDatos(response);
        } catch (Exception e) {
            ServiceException se = ServiceException.assignException(e);
            resp.addError(se);
            log.error("{} {}", trxId, ServiceResponseType.getErrorMsg(resp.getError()));
        }
        log.debug("{} END", trxId);
        log.debug("{} ----------------------------------------------------------------------------", trxId);
        return new ResponseEntity<>(resp, ServiceResponseType.getHttpStatus(resp.getError(), HttpStatus.CREATED));
    }


    @RequestMapping(value = "ceder", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "Metodo que envia la solicitud de cecion"
            , notes = "insterfaz para consumo de servicio de CedeDte"
    )
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ServiceResponseType<CedeDteResponse>> cederDte(
            @ApiParam(value = "Obtener idRespuesta request") @RequestBody CederDteRequest request,
            @ApiParam(value = "Transaccion Id (UUID)") @RequestParam(required = false) UUID trxId
    ) {
        if (trxId == null) trxId = UUID.randomUUID();
        ControllerLinkBuilder link = linkTo(methodOn(CesionController.class).cederDte(request, trxId));

        ServiceResponseType<CedeDteResponse> resp = new ServiceResponseType<>(trxId);
        log.debug("{} ----------------------------------------------------------------------------", trxId);
        try {
            log.debug("{} START", trxId);
            log.debug("{} GET   {}", trxId, link.toUri().toString());
            resp.add(link.withSelfRel());

            // Comienso del proceso
            CedeDteResponse response = cedeDteService.cederDte(request, trxId);

            resp.setDatos(response);
        } catch (Exception e) {
            ServiceException se = ServiceException.assignException(e);
            resp.addError(se);
            log.error("{} {}", trxId, ServiceResponseType.getErrorMsg(resp.getError()));
        }
        log.debug("{} END", trxId);
        log.debug("{} ----------------------------------------------------------------------------", trxId);
        return new ResponseEntity<>(resp, ServiceResponseType.getHttpStatus(resp.getError(), HttpStatus.CREATED));
    }

    @RequestMapping(value = "consultar", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "Metodo que envia la solicitud para cnsultar el estado de un AEC"
            , notes = "insterfaz para consumo de servicio de ConsultaEstadoAEC"
    )
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ServiceResponseType<ConsultaEstadoAECResponse>> consultarEstadoAEC(
            @ApiParam(value = "Obtener idRespuesta request") @RequestBody ConsultaEstadoAECRequest request,
            @ApiParam(value = "Transaccion Id (UUID)") @RequestParam(required = false) UUID trxId
    ) {
        if (trxId == null) trxId = UUID.randomUUID();
        ServiceResponseType<ConsultaEstadoAECResponse> resp = new ServiceResponseType<>(trxId);
        ControllerLinkBuilder link = linkTo(methodOn(CesionController.class).consultarEstadoAEC(request, trxId));

        log.debug("{} ----------------------------------------------------------------------------", trxId);
        try {
            log.debug("{} START", trxId);
            log.debug("{} GET   {}", trxId, link.toUri().toString());
            resp.add(link.withSelfRel());

            // Comienso del proceso
            ConsultaEstadoAECResponse response = consultaEstadoAECService.ConsultaEstadoAEC(request, trxId);

            resp.setDatos(response);
        } catch (Exception e) {
            ServiceException se = ServiceException.assignException(e);
            resp.addError(se);
            log.error("{} {}", trxId, ServiceResponseType.getErrorMsg(resp.getError()));
        }
        log.debug("{} END", trxId);
        log.debug("{} ----------------------------------------------------------------------------", trxId);
        return new ResponseEntity<>(resp, ServiceResponseType.getHttpStatus(resp.getError(), HttpStatus.CREATED));
    }

}

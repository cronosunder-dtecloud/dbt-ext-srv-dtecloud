package com.dbthor.external.dtecloud.controller;

import com.dbthor.tools.services.ServiceResponseType;
import com.dbthor.external.dtecloud.entity.request.token.ObtenerTokenRequest;
import com.dbthor.external.dtecloud.entity.request.token.ValidarTokenRequest;
import com.dbthor.external.dtecloud.entity.response.token.ObtenerTokenResponse;
import com.dbthor.external.dtecloud.entity.response.token.ValidarTokenResponse;
import com.dbthor.exception.ServiceException;
import com.dbthor.external.dtecloud.service.autentificacion.TokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Api de acceso a servicios de cesione para manejo detoken
 *
 * Created by CMIRANDA on 25-08-2017.
 */
@Api
@RestController
@RequestMapping(TokenController.BASE_URI)
@Log4j2
@SuppressWarnings("unused,WeakerAccess")
public class TokenController {
    static final String BASE_URI="/dtecloud/idRespuesta";

    final TokenService tokenService;

    //------------------------------------------------------------------------------------------------------------------
    @Autowired
    public TokenController(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Metodo para obtener el idRespuesta para las credenciales enviadas
     * @param obtenerTokenRequest
     * @param trxId
     * @return
     */
    @RequestMapping(value="obtener", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "Metodo que obtiene el idRespuesta para las credenciales enviadas."
            , notes = "Servicio obtiene el idRespuesta, se genera un idRespuesta en caso de no haber uno activo o se obtiene en caso de estar activo."
    )
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ServiceResponseType<ObtenerTokenResponse>> obtenerToken(
            @ApiParam(value = "Obtener idRespuesta request")      @RequestBody ObtenerTokenRequest obtenerTokenRequest,
            @ApiParam(value = "Transaccion Id (UUID)")      @RequestParam(required = false) UUID trxId
    ) {
        ControllerLinkBuilder link = linkTo(methodOn(TokenController.class).obtenerToken(obtenerTokenRequest, trxId ));
        if (trxId==null) trxId= UUID.randomUUID();
        ServiceResponseType<ObtenerTokenResponse> resp=  new ServiceResponseType<>(trxId);
        log.debug("{} ----------------------------------------------------------------------------", trxId);
        try {
            log.debug("{} START", trxId);
            log.debug("{} GET   {}", trxId, link.toUri().toString() );
            resp.add(link.withSelfRel());

            // Comienso del proceso
            ObtenerTokenResponse response = tokenService.getToken(obtenerTokenRequest, trxId);

            resp.setDatos(response);
        } catch (Exception e) {
            ServiceException se= ServiceException.assignException(e);
            resp.addError(se);
            log.error("{} {}", trxId, ServiceResponseType.getErrorMsg(resp.getError()));
        }
        log.debug("{} END", trxId );
        log.debug("{} ----------------------------------------------------------------------------", trxId);
        return new ResponseEntity<>(resp, ServiceResponseType.getHttpStatus(resp.getError(),HttpStatus.CREATED));
    }


    /**
     * Metodo para obtener el idRespuesta para las credenciales enviadas
     * @param validarTokenRequest
     * @param trxId
     * @return
     */
    @RequestMapping(value="consultar", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "Metodo para obtener el estado de un idRespuesta"
            , notes = "Servicio obtiene el estado de idRespuesta"
    )
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ServiceResponseType<ValidarTokenResponse>> validarToken(
            @ApiParam(value = "Obtener idRespuesta request")      @RequestBody ValidarTokenRequest validarTokenRequest,
            @ApiParam(value = "Transaccion Id (UUID)")      @RequestParam(required = false) UUID trxId
    ) {
        ControllerLinkBuilder link = linkTo(methodOn(TokenController.class).validarToken(validarTokenRequest, trxId ));
        if (trxId==null) trxId= UUID.randomUUID();
        ServiceResponseType<ValidarTokenResponse> resp=  new ServiceResponseType<>(trxId);
        log.debug("{} ----------------------------------------------------------------------------", trxId);
        try {
            log.debug("{} START", trxId);
            log.debug("{} GET   {}", trxId, link.toUri().toString() );
            resp.add(link.withSelfRel());

            // Comienso del proceso
            ValidarTokenResponse response = tokenService.validarToken(validarTokenRequest, trxId);

            resp.setDatos(response);
        } catch (Exception e) {
            ServiceException se= ServiceException.assignException(e);
            resp.addError(se);
            log.error("{} {}", trxId, ServiceResponseType.getErrorMsg(resp.getError()));
        }
        log.debug("{} END", trxId );
        log.debug("{} ----------------------------------------------------------------------------", trxId);
        return new ResponseEntity<>(resp, ServiceResponseType.getHttpStatus(resp.getError(),HttpStatus.CREATED));
    }
}

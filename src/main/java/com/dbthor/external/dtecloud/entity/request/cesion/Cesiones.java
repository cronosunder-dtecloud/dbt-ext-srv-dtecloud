package com.dbthor.external.dtecloud.entity.request.cesion;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;


/**
 * Clase Cesiones
 *
 * @author MSOTO
 * @version 1.0
 * @date 25-08-2017
 * @time 15:45
 */
@AllArgsConstructor
public class Cesiones {
    @Getter
    @Setter
    private List<UUID> listDte;
    @Getter
    @Setter
    private List<UUID> listAec;
    @Getter
    @Setter
    private UUID cedenteId;
    @Getter
    @Setter
    private UUID cedenteUsuarioId;
    @Getter
    @Setter
    private UUID cecionarioId;
    @Getter
    @Setter
    private UUID certificadoId;
    @Getter
    @Setter
    private String clave;
}
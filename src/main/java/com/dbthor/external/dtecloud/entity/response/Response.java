package com.dbthor.external.dtecloud.entity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

/**
 * Clase Response
 *
 * @author MSOTO
 * @version 1.0
 * @date 25-08-2017
 * @time 14:08
 */
public class Response {
    @Getter
    @Setter
    @JsonProperty("trxId")
    private UUID trxId;

    public Response(UUID trxId) {
        this.trxId = trxId;
    }
}

package com.dbthor.external.dtecloud.entity.response.cesion;

import com.dbthor.external.dtecloud.entity.response.Response;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

/**
 * Clase ObtenerTokenResponse
 *
 * @author MSOTO
 * @version 1.0
 * @date 24-08-2017
 * @time 18:19
 */
public class CedeDteResponse extends Response {

    @Getter
    @JsonProperty("code")
    @JsonPropertyDescription("Codigo identificador")
    private String code;
    @Getter
    @JsonProperty("mensaje")
    @JsonPropertyDescription("Descripcion del mensaje")
    private String mensaje;

    @Getter
    @Setter
    @JsonProperty("idRespuesta")
    @JsonPropertyDescription("idRespuesta obtenido de la consulta")
    private String idRespuesta;

    @Getter
    @Setter
    @JsonProperty("descripcionRespuesta")
    @JsonPropertyDescription("descripcionRespuesta obtenido de la consulta")
    private String descripcionRespuesta;



    @Getter
    @Setter
    @JsonIgnore
    private CODES actCode;

    @Getter
    @Setter
    @JsonIgnore
    private RESPUESTAS actRespuesta;

    public CedeDteResponse(UUID trxId) {
        super(trxId);
        asignarCodigo(CODES.ERR_0);
    }

    public CedeDteResponse(CODES code, UUID trxId) {
        super(trxId);
        this.code = code.name();
        this.mensaje = code.msg();
        this.actCode = code;
    }

    public void asignarCodigo(CODES code) {
        this.code = code.name();
        this.mensaje = code.msg();
        this.actCode = code;
    }

    public void asignarRespuesta(RESPUESTAS respuesta) {
        actRespuesta = respuesta;
        this.idRespuesta = actRespuesta.id();
        this.descripcionRespuesta = actRespuesta.msg();
    }

    public CODES codes() {
        return CODES.ERR_0;
    }

    public RESPUESTAS respuestas() {
        return RESPUESTAS.ERR_0;
    }

    @SuppressWarnings("unused")
    public enum CODES {
        ERR_0("SIN RESPUESTA"),
        E_TDUP("Ya Existe un Token Activo."),
        E_TVAL("Token V�lido."),
        E_TEXP("Token Expirado."),
        E_TINV("Token Invalido.");
        private String msg;

        CODES(String msg) {
            this.msg = msg;
        }

        public String msg() {
            return msg;
        }
    }

    @SuppressWarnings("unused")
    public enum RESPUESTAS {
        ERR_0("-1", "SIN RESPUESTA"),
        ERR_2("-2", "Error en datos"),
        E_0("0", "Proceso Finalizado Correctamente."),
        E_1("1", "Documento ya se encuentra cedido");
        private String msg;
        private String id;

        RESPUESTAS(String id, String msg) {
            this.msg = msg;
            this.id = id;

        }

        public String msg() {
            return msg;
        }

        public String id() {
            return id;
        }
    }

}

package com.dbthor.external.dtecloud.entity.request.token;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import lombok.Getter;
import lombok.Setter;

/**
 * Clase RegistraCedenteRequest
 *
 * @author MSOTO
 * @version 1.0
 * @date 24-08-2017
 * @time 18:11
 */
public class ObtenerTokenRequest {

    @Getter
    @Setter
    @JsonProperty("rut")
    @JsonPropertyDescription("xxxxxxxx-x")
    private String rut;

    @Getter
    @Setter
    @JsonProperty("clave")
    private String clave;

    @Override
    public String toString() {
        return "{'rut':'" + rut + "', 'clave':'xxxxxx'}";
    }
}

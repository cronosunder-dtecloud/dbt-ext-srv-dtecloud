package com.dbthor.external.dtecloud.entity.request.registra.cedente;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import lombok.Getter;
import lombok.Setter;

/**
 * Clase RegistraCedenteRequest
 *
 * @author CMIRANDA
 * @version 1.0
 * @date 25-08-2017
 * @time 18:11
 */
public class RegistraCedenteRequest {

    @Getter
    @Setter
    @JsonProperty
    private String rutEmpresa;
    @Getter
    @Setter
    @JsonProperty
    private String rutIntegrador;

    @Getter
    @Setter
    @JsonProperty("razonSocial")
    private String razonSocial;
    @Getter
    @Setter
    @JsonProperty("rutAutorizado")
    private String rutAutorizado;
    @Getter
    @Setter
    @JsonProperty("nombreAutorizado")
    private String nombreAutorizado;

    @Getter
    @Setter
    @JsonProperty("certEncode")
    @JsonPropertyDescription("Certificado dijital en .pfx  codificado en base 64")
    private String certEncode;

    @Getter
    @Setter
    @JsonProperty("certPass")
    @JsonPropertyDescription("clave del certificado enviado")
    private String certPass;

}

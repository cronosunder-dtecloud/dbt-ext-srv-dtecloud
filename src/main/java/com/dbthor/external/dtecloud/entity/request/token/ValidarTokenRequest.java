package com.dbthor.external.dtecloud.entity.request.token;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Clase RegistraCedenteRequest
 *
 * @author MSOTO
 * @version 1.0
 * @date 24-08-2017
 * @time 18:11
 */
public class ValidarTokenRequest {
    @Getter
    @Setter
    @JsonProperty("token")
    private String token;

    @Override
    public String toString() {
        return "{" +
                "idRespuesta='" + token + '\'' +
                '}';
    }
}

package com.dbthor.external.dtecloud.entity.request.cesion;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Clase RegistraCedenteRequest
 *
 * @author CMIRANDA
 * @version 1.0
 * @date 25-08-2017
 * @time 18:11
 */
public class CargarXmlBase64Request {

    @Getter
    @Setter
    @JsonProperty("TOKEN")
    private String token;
    @Getter
    @Setter
    @JsonProperty("XMLBASE64")
    private String base64encode;
}

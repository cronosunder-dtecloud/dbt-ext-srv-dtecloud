package com.dbthor.external.dtecloud.entity.response.token;

import com.dbthor.external.dtecloud.entity.response.Response;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

/**
 * Clase ObtenerTokenResponse
 *
 * @author MSOTO
 * @version 1.0
 * @date 24-08-2017
 * @time 18:19
 */
public class ValidarTokenResponse extends Response {

    @Getter
    @JsonProperty("code")
    @JsonPropertyDescription("Codigo identificador")
    private String code;
    @Getter
    @JsonProperty("mensaje")
    @JsonPropertyDescription("Descripcion del mensaje")
    private String mensaje;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("token")
    @JsonPropertyDescription("idRespuesta obtenido de la consulta")
    private String token;

    @Getter
    @Setter
    @JsonIgnore
    private CODES actCode;

    public ValidarTokenResponse(UUID trxId) {
        super(trxId);
        asignarCodigo(CODES.ERR_0);
    }

    public ValidarTokenResponse(CODES code, UUID trxId) {
        super(trxId);
        this.code = code.name();
        this.mensaje = code.msg();
        this.actCode = code;
    }

    public void asignarCodigo(CODES code) {
        this.code = code.name();
        this.mensaje = code.msg();
        this.actCode = code;
    }

    public enum CODES {
        ERR_0("SIN RESPUESTA"),
        E_TDUP("Token Generado Correctamente."),
        E_TVAL("Datos de Acceso para Generar Token Incorrectos."),
        E_TEXP("Ya Existe un Token Activo."),
        E_TINV("Token V�lido.");
        private String msg;

        CODES(String msg) {
            this.msg = msg;
        }

        public String msg() {
            return msg;
        }
    }

}

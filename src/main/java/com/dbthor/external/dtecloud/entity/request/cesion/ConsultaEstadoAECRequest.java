package com.dbthor.external.dtecloud.entity.request.cesion;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Clase RegistraCedenteRequest
 *
 * @author CMIRANDA
 * @version 1.0
 * @date 25-08-2017
 * @time 18:11
 */
//@JsonPropertyOrder({ "TOKEN", "folio", "tipoDte", "rutEmisor", "attributes" })
public class ConsultaEstadoAECRequest {

    @Getter
    @Setter
    @JsonProperty("TOKEN")
    private String token;
    @Getter
    @Setter
    @JsonProperty("folio")
    private int folio;

    @Getter
    @Setter
    @JsonProperty("tipoDte")
    private int tipoDte;
    @Getter
    @Setter
    @JsonProperty("rutEmisor")
    private String rutEmisor;
    @Getter
    @Setter
    @JsonProperty("idSecuencia")
    private int idSecuencia;
}

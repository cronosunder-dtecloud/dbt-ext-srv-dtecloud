package com.dbthor.external.dtecloud.entity.request.cesion;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Clase RegistraCedenteRequest
 *
 * @author CMIRANDA
 * @version 1.0
 * @date 25-08-2017
 * @time 18:11
 */
//@JsonPropertyOrder({ "TOKEN", "folio", "tipoDte", "rutEmisor", "attributes" })
public class CederDteRequest {

    @Getter
    @Setter
    @JsonProperty("TOKEN")
    private String token;
    @Getter
    @Setter
    @JsonProperty("folio")
    private int folio;

    @Getter
    @Setter
    @JsonProperty("tipoDte")
    private int tipoDte;
    @Getter
    @Setter
    @JsonProperty("rutEmisor")
    private String rutEmisor;
    @Getter
    @Setter
    @JsonProperty("idSecuencia")
    private int idSecuencia;
    @Getter
    @Setter
    @JsonProperty("rutCesionario")
    private String rutCesionario;
    @Getter
    @Setter
    @JsonProperty("razCesionario")
    private String razCesionario;
    @Getter
    @Setter
    @JsonProperty("dirCesionario")
    private String dirCesionario;
    @Getter
    @Setter
    @JsonProperty("emailCesionario")
    private String emailCesionario;
    @Getter
    @Setter
    @JsonProperty("nombreEjecutivo")
    private String nombreEjecutivo;
    @Getter
    @Setter
    @JsonProperty("emailEjecutivo")
    private String emailEjecutivo;
    @Getter
    @Setter
    @JsonProperty("rutAutorizadoSii")
    private String rutAutorizadoSii;
    @Getter
    @Setter
    @JsonProperty("nombreAutorizadoSii")
    private String nombreAutorizadoSii;
    @Getter
    @Setter
    @JsonProperty("fechaVencimiento")
    private String fechaVencimiento;
    @Getter
    @Setter
    @JsonProperty("emailDeudor")
    private String emailDeudor;
}

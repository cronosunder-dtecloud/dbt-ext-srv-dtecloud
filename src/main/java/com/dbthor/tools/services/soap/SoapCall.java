package com.dbthor.tools.services.soap;

import com.dbthor.exception.ServiceException;
import com.dbthor.exception.ServiceExceptionCodes;
import com.dbthor.tools.SOAPUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

import javax.xml.soap.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

/**
 * Case con la iniciacion para realizar consulta soap.
 */
@Log4j2
@SuppressWarnings("ALL")
public class SoapCall {
    @Getter
    @Setter
    private URL endpoint;
    @Getter
    private SOAPMessage message;
    @Getter
    @Setter
    private SOAPPart soapPart;
    @Getter
    @Setter
    private SOAPEnvelope envelope;
    @Getter
    @Setter
    private SOAPHeader header;
    @Getter
    @Setter
    private SOAPBody body;

    public SoapCall(URL enPoint) throws SOAPException {
        this.endpoint = enPoint;
        init();
    }


    public SoapCall(String enPoint) throws ServiceException {
        try {
            this.endpoint = new URL(enPoint.trim());
        } catch (MalformedURLException e) {
            throw new ServiceException(ServiceExceptionCodes.SERV_ERR_SOAP_URL_MAL_FORMED, e);
        }
        try {
            init();
        } catch (SOAPException e) {
            throw new ServiceException(ServiceExceptionCodes.SERV_ERR_SOAP_INIT, e);
        }
    }

    private void init() throws SOAPException {
        MessageFactory factory = MessageFactory.newInstance();
        message = factory.createMessage();
        soapPart = message.getSOAPPart();
        envelope = soapPart.getEnvelope();
        header = envelope.getHeader();
        body = envelope.getBody();
        header.detachNode();
    }

    @SuppressWarnings("unused")
    public void setMessage(SOAPMessage message) throws SOAPException {
        header.detachNode();
        this.message = message;
        soapPart = message.getSOAPPart();
        envelope = soapPart.getEnvelope();
        header = envelope.getHeader();
        body = envelope.getBody();

    }

    @SuppressWarnings("unused")
    public void addHeader(String soapAction, String s) {
        message.getMimeHeaders().addHeader(soapAction, s);
    }


    /**
     * Metodo agrega un nodo al body del mensaje
     *
     * @param bodyName {@link Name} que se agregara
     * @return SOAPBodyElement
     * @throws SOAPException Error
     */
    @SuppressWarnings("unused")
    public SOAPBodyElement addBodyElement(Name bodyName) throws SOAPException {
        return body.addBodyElement(bodyName);

    }

    @SuppressWarnings("unused")
    public SOAPMessage call(UUID trxId) throws ServiceException {
        log.debug("{} START", trxId);
        HttpSOAPConnection con;
        try {
            SOAPConnectionFactory scFactory = null;
            con = new com.dbthor.tools.services.soap.HttpSOAPConnection();
        } catch (SOAPException e) {

            if (e instanceof com.dbthor.tools.services.soap.SOAPExceptionImpl) {
                if (((com.dbthor.tools.services.soap.SOAPExceptionImpl) e).getCode() == 401) {
                    throw new ServiceException(ServiceExceptionCodes.ERROR_AUTENTIFICACION, e);
                }
            }
            throw new ServiceException(ServiceExceptionCodes.SERV_ERR_SOAP_INIT, e);

        }
        try {
            log.debug("{} URL {}", trxId, endpoint);
            SOAPUtils.soapAstring(message);

            return con.call(message, endpoint);
        } catch (SOAPException e) {
            log.debug("{} ERR", trxId);
            log.debug("{} WS SOAP URL {}", trxId, endpoint.toString());
            if (e instanceof com.dbthor.tools.services.soap.SOAPExceptionImpl) {
                if (((com.dbthor.tools.services.soap.SOAPExceptionImpl) e).getCode() == 401) {
                    throw new ServiceException(ServiceExceptionCodes.ERROR_AUTENTIFICACION, e);
                }
            }
            throw new ServiceException(ServiceExceptionCodes.SERV_ERR_SOAP_CALL, e);

        }
    }
}

package com.dbthor.tools;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.apache.commons.lang3.StringEscapeUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author MSOTO
 */
public abstract class DOMUtils {

    /**
     * Metodo convierte un String a claro
     *
     * @param text Texto de origen
     * @return {@link Document}
     * @throws SAXException                 Exception
     * @throws IOException                  Exception
     * @throws ParserConfigurationException Exception
     */
    public static Document stringToDocument(String text) throws SAXException, IOException, ParserConfigurationException {
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(text));
        return db.parse(is);
    }

    /**
     * Metodo transforma un Document a String
     *
     * @param doc Documento a transformar
     * @return un String
     */
    public static String documentToString(Document doc) {
        try {
            StringWriter sw = new StringWriter();
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
//            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
//            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
//            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            transformer.transform(new DOMSource(doc), new StreamResult(sw));
            return sw.toString();
        } catch (Exception ex) {
            throw new RuntimeException("Error converting to String", ex);
        }
    }

    /**
     * Funcion que permite filtar informacion en el String o respuesta que
     * estamos trabajando, esta funcion se llama muchas veces durante el codigo,
     * y permite hacer cualquier tipo de consulta sobre un documento XML, abre
     * un fichero en donde se encuentra la informacion que nos arroja el
     * webservice.
     *
     * @param Expresion es el String que tiene la sintaxis para ubicar algun
     *                  tipo de dato en el documento
     * @param documento Documento que se recorrera con la exprecion
     * @return el valor del campo solicitado si es que existe y si no existe
     * devuelve un valor por defecto que en este caso es vacio.
     * @throws XPathExpressionException representa un error si es que existiese
     *                                  en la expresion de tipo XPATH (libreria utilizada para realizar consultas
     *                                  a un xml)
     */
    public static String consultar(String Expresion, Document documento) throws XPathExpressionException {

//        File file = new File(directorio + getRut() + ".txt");
//        InputSource inputSource = new InputSource(new FileInputStream(file));
        XPathFactory factory = XPathFactory.newInstance();
        XPath xPath = factory.newXPath();

        XPathExpression xPathExpression = xPath.compile(Expresion.trim());

        String consulta = xPathExpression.evaluate(documento);

        if (!"".equals(consulta)) {
            return consulta;
        } else {
            return null;
        }

    }

    public static int consultarArrLeng(String Expresion, Document documento) throws XPathExpressionException {
        String respuesta = consultar(Expresion, documento);
        if(respuesta != null){
            return Integer.parseInt(respuesta);
        }else{
            throw new XPathExpressionException("ERR-CONTXML -- No se pudo obtener la cantidad de nodos con el Xpath enviado");
        }

    }

    /**
     * Firma digitalmente usando la forma "enveloped signature" segun el
     * estandar de la W3C (<a
     * href="http://www.w3.org/TR/xmldsig-core/">http://www.w3.org/TR/xmldsig-core/</a>).
     * <p>
     * <p>
     * Este metodo ademas incorpora la informacion del
     * certificado a la seccio;n &lt;KeyInfo&gt; opcional del
     * estandar, segun lo exige SII.
     * <p>
     *
     * @param doc  El documento a firmar
     * @param uri  La referencia dentro del documento que debe ser firmada
     * @param pKey La llave privada para firmar
     * @param cert El certificado digital correspondiente a la llave privada
     * @throws NoSuchAlgorithmException           Si el algoritmo de firma de la llave no
     *                                            est&aacute; soportado (Actualmente soportado RSA+SHA1, DSA+SHA1 y
     *                                            HMAC+SHA1).
     * @throws InvalidAlgorithmParameterException Si los algoritmos de
     *                                            canonizaci&oacute;n (parte del est&aacute;ndar XML Signature) no son
     *                                            soportados (actaulmente se usa el por defecto)
     * @throws KeyException                       Si hay problemas al incluir la llave p&uacute;blica
     *                                            en el &lt;KeyValue&gt;.
     * @throws MarshalException                   Exception
     * @throws XMLSignatureException              Exception
     * @see XMLSignature#sign(javax.xml.crypto.dsig.XMLSignContext)
     */
    public static void signEmbeded(Node doc, String uri, PrivateKey pKey,
                                   X509Certificate cert) throws NoSuchAlgorithmException,
            InvalidAlgorithmParameterException, KeyException, MarshalException,
            XMLSignatureException {

        // Create a DOM XMLSignatureFactory that will be used to generate the
        // enveloped signature
        XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

        // Create a Reference to the enveloped document (in this case we are
        // signing the whole document, so a URI of "" signifies that) and
        // also specify the SHA1 digest algorithm and the ENVELOPED Transform.
        Reference ref = fac.newReference(uri, fac.newDigestMethod("http://www.w3.org/2000/09/xmldsig#sha1", null), Collections.singletonList(fac.newTransform("http://www.w3.org/2000/09/xmldsig#enveloped-signature", (TransformParameterSpec) null)), null, null);

        // Create the SignedInfo
        String method = SignatureMethod.RSA_SHA1; // default by SII

        if ("DSA".equals(cert.getPublicKey().getAlgorithm())) {
            method = SignatureMethod.DSA_SHA1;
        } else if ("HMAC".equals(cert.getPublicKey().getAlgorithm())) {
            method = SignatureMethod.HMAC_SHA1;
        }

        SignedInfo si = fac.newSignedInfo(fac.newCanonicalizationMethod("http://www.w3.org/TR/2001/REC-xml-c14n-20010315", (C14NMethodParameterSpec) null), fac.newSignatureMethod(method, null), Collections.singletonList(ref));
        KeyInfoFactory kif = fac.getKeyInfoFactory();
        KeyValue kv = kif.newKeyValue(cert.getPublicKey());
        @SuppressWarnings("unchecked") List<XMLStructure> kidata = new ArrayList();
        kidata.add(kv);
        kidata.add(kif.newX509Data(Collections.singletonList(cert)));
        KeyInfo ki = kif.newKeyInfo(kidata);
        DOMSignContext dsc = new DOMSignContext(pKey, doc);
        XMLSignature signature = fac.newXMLSignature(si, ki);
        signature.sign(dsc);
    }

    @SuppressWarnings("deprecation")
    public static String scape(String text) {
        return StringEscapeUtils.escapeXml(text);
    }

}

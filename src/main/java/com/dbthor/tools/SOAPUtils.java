package com.dbthor.tools;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * @author MSOTO
 */
public abstract class SOAPUtils extends DOMUtils {

    public static SOAPMessage obtenerSoap(String xml) throws SOAPException, IOException {
        MessageFactory factory = MessageFactory.newInstance();
        return factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(xml.getBytes(Charset.forName("UTF-8"))));
    }

    public static String soapAstring(SOAPMessage message) {
        String result = null;

        if (message != null) {
            ByteArrayOutputStream baos = null;
            try {
                baos = new ByteArrayOutputStream();
                message.writeTo(baos);
                result = baos.toString();
            } catch (Exception ignored) {
            } finally {
                if (baos != null) {
                    try {
                        baos.close();
                    } catch (IOException ignored) {
                    }
                }
            }
        }

        return result;
    }

    public static Document soapToDocument(SOAPMessage responseSII) throws ParserConfigurationException, SAXException, IOException {
        return stringToDocument(soapAstring(responseSII)
        );
    }
}

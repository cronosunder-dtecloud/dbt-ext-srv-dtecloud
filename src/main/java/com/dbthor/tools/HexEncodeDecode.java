package com.dbthor.tools;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import java.io.UnsupportedEncodingException;

/**
 * Rutinas de codigicación y decodificación hexadecimal.
 *
 * @author csattler
 */
@SuppressWarnings("ALL")
public class HexEncodeDecode {

    /**
     * Convierte un string a encode hexadecimal.
     *
     * @param source    String a convertir
     * @return          char[] con el codigo hexadecimal
     */
    public static char[] getHex(String source)  {
        return Hex.encodeHex(source.getBytes());
    }

    /**
     * Convierte un codigo hexadecimal a string.
     *
     * @param hex   Codigo Hexadecimal
     * @return      String decodificado
     * @throws DecoderException             DecoderException
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
    public static String getString(char[] hex) throws DecoderException {
        return  new String(Hex.decodeHex(hex));
    }

}

package com.dbthor.exception;

/**
 * Enum de codigos de Error del dominio
 * <p>
 * Created by csattler on 19/04/2017.
 */
public enum ServiceExceptionCodes {
    //Codigo Genericos
    ERROR_NO_ADMINISTRADO("ERROR_NO_ADMINISTRADO", "Error no administrado"),
    ERROR_AUTENTIFICACION("SERV_ERROR_AUTH", "Error 401 - Autentificacion no es valida"),
    ERROR_SERVICIO_REQUERIDO("ERROR_SERVICIO_REQUERIDO", "Erroren uno de los servicios externos requeridos por el proceso"),
    JSON_RESPUESTA_INVALIDO("JSON_RESPUESTA_INVALIDO", "Respuesta JSON invalida o mal formada"),
    REQUERIMIENTO_PARAMETROS_INVALIDOS("REQUERIMIENTO_PARAMETROS_INVALIDOS", "Parametros de invocacion no cumplen regla"),
    RESPUESTA_SERVICIO_INVALIDA("RESPUESTA_SERVICIO_INVALIDA", "Respuesta de servicio web invalido o nula."),
    POST_BODY_REQUEST_NULO("POST_REQUEST_NULO", "El Body del POST request es nulo"),
    REQUERIMIENTO_INVALIDO("REQUERIMIENTO_INVALIDO", "Error en la invocacion del Servicio"),
    NO_EXISTE("NO_EXISTE", "No existe entidad/elemento/objeto"),
    MISSING_DATA("MISSING_DATA", "Invalido o faltan datos"),
    BUSINESS_RULE_ERROR("BUSINESS_RULE_ERROR", "Error regla de negocio"),

    //SOAP
    SERV_ERR_SOAP_INIT("SERV_ERR_SOAP_INIT", "Ocurrio un error al inicializar las utilidades para consultar via SOAP"),
    SERV_ERR_SOAP_CALL("SERV_ERR_SOAP_CALL", "Ocurrio un error al realizar la consulta via SOAP"),
    SERV_ERR_SOAP_URL_MAL_FORMED("SERV_ERR_SOAP_URL_MAL_FORMED", "La url a la cual se requiere consultar esta mal formada"),
    SERV_ERR_SOAP_MENSAJE("SERV_ERR_SOAP_MENSAJE", "Ocurrio un error al contruir el mensaje SOAP"),
    SERV_ERR_SOAP_MENSAJE_LEER("SERV_ERR_SOAP_MENSAJE_LEER", "Ocurrio un error al leer respuesta de SOAP"),

    //          SERVICES            ////////////////////////////////////////////////////////////////////////////
    //CERTIFICADO
    CERTIFICADO_NO_EXISTE("CERTIFICADO_NO_EXISTE","No se pudo encontrar el certificado."),
    CERTIFICADO_SIN_CONTRASENNA("CERTIFICADO_SIN_CONTRASENNA","Se nesesita contrasenna para utilizar el certificado"),
    CERTIFICADO_VENCIDO("CERTIFICADO_VENCIDO","El certificado enviado esta vencido"),

    //PERSONA
    PERSONA_NO_EXISTE("PERSONA_NO_EXISTE", "No se pudo encontrar la persona.");


    private final String id;
    private final String msg;

    ServiceExceptionCodes(String id, String msg) {
        this.id = id;
        this.msg = msg;
    }

    public String getId() {
        return this.id;
    }

    public String getMsg() {
        return this.msg;
    }

    public String toString() {
        return this.id + " - " + this.msg;
    }
}

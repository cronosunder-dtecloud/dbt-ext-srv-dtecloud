package com.dbthor.domain.cliente.certificado.service;

import com.dbthor.domain.cliente.certificado.entity.CertificadoDigital;
import com.dbthor.domain.cliente.certificado.entity.ECertificadoDigital;
import com.dbthor.exception.ServiceException;
import com.dbthor.exception.ServiceExceptionCodes;
import com.dbthor.tools.services.rest.RestCall;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@Log4j2
public class CertificadoService {

    @Value("${url.srv.certificado.get}")
    private String getCertificadoUrl;

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Obtien un certiicado cargado en un KeyStore
     *
     * @param certificadoId Identificacion del certificado
     * @param trxId         Identificacion de Transaccion
     * @param passwordCert  Password para utilizar certificado, este parametro es opcional siempre y cuando este este en BD asociado al certificado, en caso contrario de no encontrar ninguno de los dos lanzara exepcion
     * @return {@link CertificadoDigital} Clase para manejar un certificadoDijital
     * @throws ServiceException ServiceException
     */
    public CertificadoDigital getCertificado(UUID certificadoId,  String passwordCert, UUID trxId) throws ServiceException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        CertificadoDigital cert;
        ECertificadoDigital eCertificadoDigital = getCertificado(certificadoId, trxId);
        String pass;
        if (passwordCert != null && passwordCert.length() > 0) {
            pass = passwordCert;
        } else if (eCertificadoDigital.getPasswordVal() != null && eCertificadoDigital.getPasswordVal().length() > 0) {
            pass = eCertificadoDigital.getPasswordVal();
        } else {
            throw new ServiceException(ServiceExceptionCodes.CERTIFICADO_SIN_CONTRASENNA);
        }
        if (eCertificadoDigital.getExpiracionFchhr().getTime() <= new Date().getTime()) {
            throw new ServiceException(ServiceExceptionCodes.CERTIFICADO_VENCIDO);
        }

        cert = new CertificadoDigital();
        cert.loadCertificado(eCertificadoDigital.getDataEncode64Val(), eCertificadoDigital.getUsuarioCorreoVal(), pass, trxId);
        return cert;
    }


    /**
     * Obtien los datos del certificado
     *
     * @param certificadoId Identificacion del certificado
     * @param trxId         Identificacion de Transaccion
     * @return ECertificadoDigital Entidad de sertificado digital
     * @throws ServiceException ServiceException
     */
    @SuppressWarnings("unused")
    public ECertificadoDigital getCertificado(UUID certificadoId, UUID trxId) throws ServiceException {
        log.debug("{} START", trxId);
        log.debug("{} PARAM certificadoId : {}", trxId, certificadoId);

        RestCall<ECertificadoDigital> restCall = new RestCall<>();
        try {
            URIBuilder url = new URIBuilder();
            url.setPath(getCertificadoUrl.replace("{certificadoId}", certificadoId.toString()));
            url.addParameter("trxId", trxId.toString());

            log.debug("{} WS URL: {}", trxId, url.toString());
            String jsonResponse = restCall.callGet(url, trxId);

            ECertificadoDigital response = (new ObjectMapper()).readValue(jsonResponse, new TypeReference<ECertificadoDigital>() {
            });


            log.debug("{} END", trxId);
            return response;
        } catch (Exception e) {
            ServiceException fex = (e instanceof ServiceException ? (ServiceException) e : new ServiceException(ServiceExceptionCodes.ERROR_NO_ADMINISTRADO, e));
            log.error("{} {}", trxId, fex.getErrorLog());
            log.debug("{} END", trxId);
            throw fex;
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Obtiene la lista de certificados asociados a una persona.
     *
     * @param personaId Identificación de la persona
     * @param usoId     Tipo deuso del certificado
     * @param trxId     Identificación de la transacción
     * @return List<CertificadoDigital>
     * @throws ServiceException ServiceException
     */
    @SuppressWarnings("unused")
    public List<CertificadoDigital> getCertificadoPersona(UUID personaId, String usoId, UUID trxId) throws ServiceException {
        log.debug("{} START", trxId);
        log.debug("{} PARAM personaId     : {}", trxId, personaId);

        RestCall<List<CertificadoDigital>> restCall = new RestCall<>();
        try {
            URIBuilder url = new URIBuilder();
            url.setPath(getCertificadoUrl.replace("{personaId}", personaId.toString()));
            if (usoId != null) url.addParameter("usoId", usoId);
            url.addParameter("trxId", trxId.toString());

            log.debug("{} WS URL: {}", trxId, url.toString());
            String jsonResponse = restCall.callGet(url, trxId);

            List<CertificadoDigital> response = (new ObjectMapper()).readValue(jsonResponse, new TypeReference<List<CertificadoDigital>>() {
            });

            log.debug("{} END", trxId);
            return response;
        } catch (Exception e) {
            ServiceException fex = ServiceException.assignException(e);
            log.error("{} {}", trxId, fex.getErrorLog());
            log.debug("{} END", trxId);
            throw fex;
        }
    }
}

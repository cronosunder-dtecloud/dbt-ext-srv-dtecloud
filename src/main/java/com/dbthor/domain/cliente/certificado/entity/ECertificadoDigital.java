package com.dbthor.domain.cliente.certificado.entity;


import com.dbthor.tools.DateTools;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Clase ECertificadoDigital
 *
 * @author MSOTO
 * @version 1.0
 * @date 30-08-2017
 * @time 9:56
 */

public class ECertificadoDigital {
    private String id;
    private String dataEncode64Val;
    private String archivoNombre;
    private String usuarioCorreoVal;
    private String passwordVal;
    private Timestamp creacionFchhr;
    private Timestamp expiracionFchhr;
    private String subjectDnVal;
    private String issuerDnVal;
    private Timestamp regCreacionFchhr = DateTools.convertUtil2SqlTimestamp(new Date());

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDataEncode64Val() {
        return dataEncode64Val;
    }

    public void setDataEncode64Val(String dataEncode64Val) {
        this.dataEncode64Val = dataEncode64Val;
    }

    public String getArchivoNombre() {
        return archivoNombre;
    }

    public void setArchivoNombre(String archivoNombre) {
        this.archivoNombre = archivoNombre;
    }


    public String getUsuarioCorreoVal() {
        return usuarioCorreoVal;
    }

    public void setUsuarioCorreoVal(String usuarioCorreoVal) {
        this.usuarioCorreoVal = usuarioCorreoVal;
    }

    public String getPasswordVal() {
        return passwordVal;
    }

    public void setPasswordVal(String passwordVal) {
        this.passwordVal = passwordVal;
    }

    public Timestamp getCreacionFchhr() {
        return creacionFchhr;
    }

    public void setCreacionFchhr(Timestamp creacionFchhr) {
        this.creacionFchhr = creacionFchhr;
    }

    public Timestamp getExpiracionFchhr() {
        return expiracionFchhr;
    }

    public void setExpiracionFchhr(Timestamp expiracionFchhr) {
        this.expiracionFchhr = expiracionFchhr;
    }

    public String getSubjectDnVal() {
        return subjectDnVal;
    }

    public void setSubjectDnVal(String subjectDnVal) {
        this.subjectDnVal = subjectDnVal;
    }

    public String getIssuerDnVal() {
        return issuerDnVal;
    }

    public void setIssuerDnVal(String issuerDnVal) {
        this.issuerDnVal = issuerDnVal;
    }

    public Timestamp getRegCreacionFchhr() {
        return regCreacionFchhr;
    }

    public void setRegCreacionFchhr(Timestamp regCreacionFchhr) {
        this.regCreacionFchhr = regCreacionFchhr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ECertificadoDigital that = (ECertificadoDigital) o;

        return (id != null ? id.equals(that.id) : that.id == null)
                && (dataEncode64Val != null ? dataEncode64Val.equals(that.dataEncode64Val) : that.dataEncode64Val == null)
                && (usuarioCorreoVal != null ? usuarioCorreoVal.equals(that.usuarioCorreoVal) : that.usuarioCorreoVal == null)
                && (creacionFchhr != null ? creacionFchhr.equals(that.creacionFchhr) : that.creacionFchhr == null)
                && (expiracionFchhr != null ? expiracionFchhr.equals(that.expiracionFchhr) : that.expiracionFchhr == null)
                && (subjectDnVal != null ? subjectDnVal.equals(that.subjectDnVal) : that.subjectDnVal == null)
                && (issuerDnVal != null ? issuerDnVal.equals(that.issuerDnVal) : that.issuerDnVal == null)
                && (regCreacionFchhr != null ? regCreacionFchhr.equals(that.regCreacionFchhr) : that.regCreacionFchhr == null);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (dataEncode64Val != null ? dataEncode64Val.hashCode() : 0);
        result = 31 * result + (usuarioCorreoVal != null ? usuarioCorreoVal.hashCode() : 0);
        result = 31 * result + (creacionFchhr != null ? creacionFchhr.hashCode() : 0);
        result = 31 * result + (expiracionFchhr != null ? expiracionFchhr.hashCode() : 0);
        result = 31 * result + (subjectDnVal != null ? subjectDnVal.hashCode() : 0);
        result = 31 * result + (issuerDnVal != null ? issuerDnVal.hashCode() : 0);
        result = 31 * result + (regCreacionFchhr != null ? regCreacionFchhr.hashCode() : 0);
        return result;
    }
}

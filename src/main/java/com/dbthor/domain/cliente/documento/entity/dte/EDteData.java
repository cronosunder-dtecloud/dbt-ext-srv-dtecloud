package com.dbthor.domain.cliente.documento.entity.dte;

import com.dbthor.tools.DateTools;

import java.sql.Timestamp;

/**
 * Clase EDteData
 *
 * @author MSOTO
 * @version 1.0
 * @date 25-08-2017
 * @time 17:33
 */
public class EDteData {
    private String dteId;
    private String dteDataEncode64Val;
    private String fileName;
    private Timestamp regCreacionFchhr = DateTools.convertUtil2SqlTimestamp(new java.util.Date());
    private EDte dteByDteId;

    public String getDteId() {
        return dteId;
    }

    public void setDteId(String dteId) {
        this.dteId = dteId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDteDataEncode64Val() {
        return dteDataEncode64Val;
    }

    public void setDteDataEncode64Val(String dteDataEncode64Val) {
        this.dteDataEncode64Val = dteDataEncode64Val;
    }

    public Timestamp getRegCreacionFch() {
        return regCreacionFchhr;
    }

    public void setRegCreacionFch(Timestamp regCreacionFchhr) {
        this.regCreacionFchhr = regCreacionFchhr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EDteData eDteData = (EDteData) o;

        if (dteId != null ? !dteId.equals(eDteData.dteId) : eDteData.dteId != null) return false;
        if (dteDataEncode64Val != null ? !dteDataEncode64Val.equals(eDteData.dteDataEncode64Val) : eDteData.dteDataEncode64Val != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dteId != null ? dteId.hashCode() : 0;
        result = 31 * result + (dteDataEncode64Val != null ? dteDataEncode64Val.hashCode() : 0);
        return result;
    }

    public EDte getDteByDteId() {
        return dteByDteId;
    }

    public void setDteByDteId(EDte dteByDteId) {
        this.dteByDteId = dteByDteId;
    }
}
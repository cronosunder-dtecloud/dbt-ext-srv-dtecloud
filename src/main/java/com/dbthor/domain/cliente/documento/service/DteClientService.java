package com.dbthor.domain.cliente.documento.service;


import com.dbthor.domain.cliente.documento.entity.aec.EAecData;
import com.dbthor.domain.cliente.documento.entity.dte.EDte;
import com.dbthor.domain.cliente.documento.entity.dte.EDteData;
import com.dbthor.exception.ServiceException;
import com.dbthor.tools.services.rest.RestCall;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Clase DteClientService
 *
 * @author MSOTO
 * @version 1.0
 * @date 25-08-2017
 * @time 17:43
 */
@Service
@Log4j2
public class DteClientService {

    @Value("${url.srv.dte.get}")
    private String getDteUrl;
    @Value("${url.srv.dte.data.get}")
    private String getDteDataUrl;
    @Value("${url.srv.dte.buscar.get}")
    private String getDteBuscarUrl;


    public EAecData getAecData(UUID aecId, UUID trxId) {
        return new EAecData();
    }


    //------------------------------------------------------------------------------------------------------------------

    /**
     * Obtiene un Dte apartir de su Id
     *
     * @param dteId Identificación del Dte
     * @param trxId Identificación de la transacción
     * @return EDte
     * @throws ServiceException ServiceException
     */
    public EDte getDte(UUID dteId, UUID trxId) throws ServiceException {
        log.debug("{} START", trxId);
        log.debug("{} PARAM dteId   : {}", trxId, dteId);
        RestCall<EDteData> restCall = new RestCall<>();
        try {
            URIBuilder url = new URIBuilder();
            url.setPath(getDteUrl.replace("{dteId}", dteId.toString()));
            url.addParameter("trxId", trxId.toString());

            log.debug("{} WS URL: {}", trxId, url.toString());

            String jsonResponse = restCall.callGet(url, trxId);

            EDte response = (new ObjectMapper()).readValue(jsonResponse, new TypeReference<EDte>() {
            });

            log.debug("{} END", trxId);
            return response;
        } catch (Exception e) {
            ServiceException se = ServiceException.assignException(e);
            log.error("{} {}", trxId, se.getErrorLog());
            log.debug("{} END", trxId);
            throw se;
        }
    }


    //------------------------------------------------------------------------------------------------------------------

    /**
     * Obtiene el XML de un dte
     *
     * @param dteId Identificación del Dte
     * @param trxId Identificación de la transacción
     * @return EDte
     * @throws ServiceException ServiceException
     */
    public EDteData getDteData(UUID dteId, UUID trxId) throws ServiceException {
        log.debug("{} START", trxId);
        log.debug("{} PARAM dteId   : {}", trxId, dteId);
        RestCall<EDteData> restCall = new RestCall<>();
        try {
            URIBuilder url = new URIBuilder();
            url.setPath(getDteDataUrl.replace("{dteId}", dteId.toString()));
            url.addParameter("trxId", trxId.toString());

            log.debug("{} WS URL: {}", trxId, url.toString());

            String jsonResponse = restCall.callGet(url, trxId);

            EDteData response = (new ObjectMapper()).readValue(jsonResponse, new TypeReference<EDteData>() {
            });

            log.debug("{} END", trxId);
            return response;
        } catch (Exception e) {
            ServiceException se = ServiceException.assignException(e);
            log.error("{} {}", trxId, se.getErrorLog());
            log.debug("{} END", trxId);
            throw se;
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Buscar un Dte apartir de su tipo dte, número de folio y rut emisor
     *
     * @param tipoDte   Tipo de dte
     * @param folioNum  Número de folio
     * @param emisorRut Rut del emisor
     * @param trxId     Idenificación de la transacción
     * @return EDte
     * @throws ServiceException ServiceException
     */
    public EDte searchDte(Integer tipoDte, Long folioNum, String emisorRut, UUID trxId) throws ServiceException {
        log.debug("{} START", trxId);
        log.debug("{} PARAM tipoDte   : {}", trxId, tipoDte);
        log.debug("{} PARAM folioNum  : {}", trxId, folioNum);
        log.debug("{} PARAM emisorRut : {}", trxId, emisorRut);
        RestCall<EDte> restCall = new RestCall<>();
        try {
            URIBuilder url = new URIBuilder();
            url.setPath(getDteBuscarUrl);
            url.addParameter("tipoDte", tipoDte.toString());
            url.addParameter("folioNum", folioNum.toString());
            url.addParameter("emisorRut", emisorRut);
            url.addParameter("trxId", trxId.toString());

            log.debug("{} WS URL: {}", trxId, url.toString());

            String jsonResponse = restCall.callGet(url, trxId);

            EDte response = (new ObjectMapper()).readValue(jsonResponse, new TypeReference<EDte>() {
            });

            log.debug("{} END", trxId);
            return response;
        } catch (Exception e) {
            ServiceException se = ServiceException.assignException(e);
            log.error("{} {}", trxId, se.getErrorLog());
            log.debug("{} END", trxId);
            throw se;
        }
    }

}

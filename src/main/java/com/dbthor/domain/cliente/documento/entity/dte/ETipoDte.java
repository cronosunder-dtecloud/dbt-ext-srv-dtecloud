package com.dbthor.domain.cliente.documento.entity.dte;

/**
 * Clase ETipoDte
 *
 * @author MSOTO
 * @version 1.0
 * @date 25-08-2017
 * @time 17:36
 */
public class ETipoDte {
    private Integer id;
    private String descripcion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ETipoDte eTipoDte = (ETipoDte) o;

        if (id != null ? !id.equals(eTipoDte.id) : eTipoDte.id != null) return false;
        if (descripcion != null ? !descripcion.equals(eTipoDte.descripcion) : eTipoDte.descripcion != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        return result;
    }
}
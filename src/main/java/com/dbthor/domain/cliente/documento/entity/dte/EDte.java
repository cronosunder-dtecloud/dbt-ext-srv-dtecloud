package com.dbthor.domain.cliente.documento.entity.dte;

import com.dbthor.tools.DateTools;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Clase EDte
 *
 * @author MSOTO
 * @version 1.0
 * @date 25-08-2017
 * @time 17:35
 */
public class EDte {
    private String id;
    private Long folioNum;
    private String emisorVal;
    private String emisorNombre;
    private String deudorVal;
    private String deudorNombre;
    private BigInteger montoTotalVal;
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", locale = "es-CL", timezone = "America/Santiago")
    private Date emisionFch;
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", locale = "es-CL", timezone = "America/Santiago")
    private Date vencimientoFch;

    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", locale = "es-CL", timezone = "America/Santiago")
    private Date pagoFch;
    private Integer dteValidacionGrupoId;
    private Timestamp regCreacionFchhr = DateTools.convertUtil2SqlTimestamp(new java.util.Date());
    private ETipoDte tipoDteByTipoDteId;

    /*----------------------------------------------------------------------------------------------------------------*/
    public EDte () {}

    public EDte (String id) { setId(id);}

    public EDte (String emisorVal, Long folioNum,Date emisionFch) {
        setEmisorVal(emisorVal);
        setFolioNum(folioNum);
        setEmisionFch(emisionFch);
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getFolioNum() {
        return folioNum;
    }

    public void setFolioNum(Long folioNum) {
        this.folioNum = folioNum;
    }

    public String getEmisorVal() {
        return emisorVal;
    }

    public void setEmisorVal(String emisorVal) {
        this.emisorVal = emisorVal;
    }

    public String getEmisorNombre() {
        return emisorNombre;
    }

    public void setEmisorNombre(String emisorNombre) {
        this.emisorNombre = emisorNombre;
    }

    public String getDeudorVal() {
        return deudorVal;
    }

    public void setDeudorVal(String deudorVal) {
        this.deudorVal = deudorVal;
    }

    public String getDeudorNombre() {
        return deudorNombre;
    }

    public void setDeudorNombre(String deudorNombre) {
        this.deudorNombre = deudorNombre;
    }


    public Date getVencimientoFch() {
        return vencimientoFch;
    }

    public void setVencimientoFch(Date vencimientoFch) {
        this.vencimientoFch = vencimientoFch;
    }

    public Date getPagoFch() {
        return pagoFch;
    }

    public void setPagoFch(Date pagoFch) {
        this.pagoFch = pagoFch;
    }


    public Date getEmisionFch() {
        return emisionFch;
    }

    public void setEmisionFch(Date emisionFch) {
        this.emisionFch = emisionFch;
    }

    public BigInteger getMontoTotalVal() {
        return montoTotalVal;
    }

    public void setMontoTotalVal(BigInteger montoTotalVal) {
        this.montoTotalVal = montoTotalVal;
    }


    public Timestamp getRegCreacionFch() {
        return regCreacionFchhr;
    }

    public void setRegCreacionFch(Timestamp regCreacionFchhr) {
        this.regCreacionFchhr = regCreacionFchhr;
    }


    public Integer getDteValidacionGrupoId() {
        return dteValidacionGrupoId;
    }

    public void setDteValidacionGrupoId(Integer dteValidacionGrupoId) {
        this.dteValidacionGrupoId = dteValidacionGrupoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EDte eDte = (EDte) o;

        if (id != null ? !id.equals(eDte.id) : eDte.id != null) return false;
        if (folioNum != null ? !folioNum.equals(eDte.folioNum) : eDte.folioNum != null) return false;
        if (emisorVal != null ? !emisorVal.equals(eDte.emisorVal) : eDte.emisorVal != null) return false;
        if (emisionFch != null ? !emisionFch.equals(eDte.emisionFch) : eDte.emisionFch != null) return false;
        if (dteValidacionGrupoId != null ? !dteValidacionGrupoId.equals(eDte.dteValidacionGrupoId) : eDte.dteValidacionGrupoId != null) return false;
        if (regCreacionFchhr != null ? !regCreacionFchhr.equals(eDte.regCreacionFchhr) : eDte.regCreacionFchhr != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (folioNum != null ? folioNum.hashCode() : 0);
        result = 31 * result + (emisorVal != null ? emisorVal.hashCode() : 0);
        result = 31 * result + (dteValidacionGrupoId != null ? dteValidacionGrupoId.hashCode() : 0);
        result = 31 * result + (emisionFch != null ? emisionFch.hashCode() : 0);
        return result;
    }

    public ETipoDte getTipoDteByTipoDteId() {
        return tipoDteByTipoDteId;
    }

    public void setTipoDteByTipoDteId(ETipoDte tipoDteByTipoDteId) {
        this.tipoDteByTipoDteId = tipoDteByTipoDteId;
    }
}

package com.dbthor.domain.cliente.documento.entity.aec;

import com.dbthor.tools.DateTools;

import java.sql.Timestamp;

/**
 * Clase EAecData
 *
 * @author MSOTO
 * @version 1.0
 * @date 25-08-2017
 * @time 17:39
 */
public class EAecData {
    private String aecId;
    private String dataEncode64Val;
    private Timestamp regCreacionFchhr = DateTools.convertUtil2SqlTimestamp(new java.util.Date());

    public String getAecId() {
        return aecId;
    }

    public void setAecId(String aecId) {
        this.aecId = aecId;
    }


    public String getDataEncode64Val() {
        return dataEncode64Val;
    }

    public void setDataEncode64Val(String dataEncode64Val) {
        this.dataEncode64Val = dataEncode64Val;
    }

    public Timestamp getRegCreacionFchhr() {
        return regCreacionFchhr;
    }

    public void setRegCreacionFchhr(Timestamp regCreacionFchhr) {
        this.regCreacionFchhr = regCreacionFchhr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EAecData eAecData = (EAecData) o;

        if (aecId != null ? !aecId.equals(eAecData.aecId) : eAecData.aecId != null) return false;
        if (dataEncode64Val != null ? !dataEncode64Val.equals(eAecData.dataEncode64Val) : eAecData.dataEncode64Val != null)
            return false;
        if (regCreacionFchhr != null ? !regCreacionFchhr.equals(eAecData.regCreacionFchhr) : eAecData.regCreacionFchhr != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = aecId != null ? aecId.hashCode() : 0;
        result = 31 * result + (dataEncode64Val != null ? dataEncode64Val.hashCode() : 0);
        result = 31 * result + (regCreacionFchhr != null ? regCreacionFchhr.hashCode() : 0);
        return result;
    }

}

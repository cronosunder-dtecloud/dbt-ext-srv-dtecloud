package com.dbthor.domain.cliente.persona.entity;

/**
 * Clase Pais
 *
 * @author MSOTO
 * @version 1.0
 * @date 25-08-2017
 * @time 16:44
 */
public class Pais {
    private String id;
    private String iso2;
    private String descripcion;
    private String nacionalidad;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIso2() {
        return iso2;
    }

    public void setIso2(String iso2) {
        this.iso2 = iso2;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pais ePais = (Pais) o;

        if (id != null ? !id.equals(ePais.id) : ePais.id != null) return false;
        if (iso2 != null ? !iso2.equals(ePais.iso2) : ePais.iso2 != null) return false;
        if (descripcion != null ? !descripcion.equals(ePais.descripcion) : ePais.descripcion != null) return false;
        if (nacionalidad != null ? !nacionalidad.equals(ePais.nacionalidad) : ePais.nacionalidad != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (iso2 != null ? iso2.hashCode() : 0);
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        result = 31 * result + (nacionalidad != null ? nacionalidad.hashCode() : 0);
        return result;
    }
}

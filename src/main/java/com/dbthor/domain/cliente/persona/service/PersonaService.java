package com.dbthor.domain.cliente.persona.service;


import com.dbthor.domain.cliente.persona.entity.Identificacion;
import com.dbthor.exception.ServiceException;
import com.dbthor.exception.ServiceExceptionCodes;
import com.dbthor.tools.services.rest.RestCall;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.UUID;
/**
 * Clase PersonaService
 *
 * @author MSOTO
 * @version 1.0
 * @date 25-08-2017
 * @time 15:51
 */
@Service
@Log4j2
public class PersonaService {


    @Value("${url.srv.persona.ident.buscar.get}") private String getIdentBuscarUrl;

    public Identificacion getIdentificacionold(UUID personaId, Integer tipoIdent, UUID trxId) {
        log.debug("{} START", trxId);
        Identificacion ident =  new Identificacion();

        ident.setIdentificacionVal("76283656");
        ident.setIdentificacionValidadorVal("4");

        log.debug("{} END", trxId);
        return ident;
    }

    public Identificacion getIdentificacion(UUID personaId, Integer tipoIdent, UUID trxId) throws ServiceException {
        log.debug("{} START", trxId);
        log.debug("{} PARAM personaId : {}", trxId, personaId);

        RestCall<Identificacion> restCall =  new RestCall<>();
        try {
            URIBuilder url = new URIBuilder();
            url.setPath(getIdentBuscarUrl);
            url.addParameter("personaId", personaId.toString());
            url.addParameter("tipoIdentificacionId", tipoIdent.toString());
            url.addParameter("trxId", trxId.toString());

            log.debug("{} WS URL: {}", trxId, url.toString());

            String jsonResponse = restCall.callGet(url,trxId);

            Identificacion response = (new ObjectMapper()).readValue(jsonResponse, new TypeReference<Identificacion>() {});

            log.debug("{} END", trxId);
            return response;
        } catch (Exception e) {
            ServiceException fex=(e instanceof ServiceException ?(ServiceException) e: new ServiceException(ServiceExceptionCodes.ERROR_NO_ADMINISTRADO, e));
            log.error("{} {}", trxId, fex.getErrorLog());
            log.debug("{} END", trxId);
            throw fex;
        }
    }
}

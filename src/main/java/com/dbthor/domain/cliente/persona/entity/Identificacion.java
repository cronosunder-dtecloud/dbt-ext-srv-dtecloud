package com.dbthor.domain.cliente.persona.entity;


import java.sql.Timestamp;

/**
 * Clase Identificacion
 *
 * @author MSOTO
 * @version 1.0
 * @date 25-08-2017
 * @time 16:42
 */
public class Identificacion {
    private String personaId;
    private Short tipoIdentificacionId;
    private Timestamp vigenciaInicioFch;
    private String identificacionVal;
    private String identificacionValidadorVal;
    private String identificacionSerieNum;
    private Timestamp vigenciaFinFch;
    private Pais paisByPaisEmisionId;

    public String getPersonaId() {
        return personaId;
    }

    public void setPersonaId(String personaId) {
        this.personaId = personaId;
    }

    public Short getTipoIdentificacionId() {
        return tipoIdentificacionId;
    }

    public void setTipoIdentificacionId(Short tipoIdentificacionId) {
        this.tipoIdentificacionId = tipoIdentificacionId;
    }

    public Timestamp getVigenciaInicioFch() {
        return vigenciaInicioFch;
    }

    public void setVigenciaInicioFch(Timestamp vigenciaInicioFch) {
        this.vigenciaInicioFch = vigenciaInicioFch;
    }

    public String getIdentificacionVal() {
        return identificacionVal;
    }

    public void setIdentificacionVal(String identificacionVal) {
        this.identificacionVal = identificacionVal;
    }

    public String getIdentificacionValidadorVal() {
        return identificacionValidadorVal;
    }

    public void setIdentificacionValidadorVal(String identificacionValidadorVal) {
        this.identificacionValidadorVal = identificacionValidadorVal;
    }

    public String getIdentificacionSerieNum() {
        return identificacionSerieNum;
    }

    public void setIdentificacionSerieNum(String identificacionSerieNum) {
        this.identificacionSerieNum = identificacionSerieNum;
    }

    public Timestamp getVigenciaFinFch() {
        return vigenciaFinFch;
    }

    public void setVigenciaFinFch(Timestamp vigenciaFinFch) {
        this.vigenciaFinFch = vigenciaFinFch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Identificacion that = (Identificacion) o;

        if (personaId != null ? !personaId.equals(that.personaId) : that.personaId != null) return false;
        if (tipoIdentificacionId != null ? !tipoIdentificacionId.equals(that.tipoIdentificacionId) : that.tipoIdentificacionId != null)
            return false;
        if (vigenciaInicioFch != null ? !vigenciaInicioFch.equals(that.vigenciaInicioFch) : that.vigenciaInicioFch != null)
            return false;
        if (identificacionVal != null ? !identificacionVal.equals(that.identificacionVal) : that.identificacionVal != null)
            return false;
        if (identificacionValidadorVal != null ? !identificacionValidadorVal.equals(that.identificacionValidadorVal) : that.identificacionValidadorVal != null)
            return false;
        if (identificacionSerieNum != null ? !identificacionSerieNum.equals(that.identificacionSerieNum) : that.identificacionSerieNum != null)
            return false;
        if (vigenciaFinFch != null ? !vigenciaFinFch.equals(that.vigenciaFinFch) : that.vigenciaFinFch != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = personaId != null ? personaId.hashCode() : 0;
        result = 31 * result + (tipoIdentificacionId != null ? tipoIdentificacionId.hashCode() : 0);
        result = 31 * result + (vigenciaInicioFch != null ? vigenciaInicioFch.hashCode() : 0);
        result = 31 * result + (identificacionVal != null ? identificacionVal.hashCode() : 0);
        result = 31 * result + (identificacionValidadorVal != null ? identificacionValidadorVal.hashCode() : 0);
        result = 31 * result + (identificacionSerieNum != null ? identificacionSerieNum.hashCode() : 0);
        result = 31 * result + (vigenciaFinFch != null ? vigenciaFinFch.hashCode() : 0);
        return result;
    }

    public Pais getPaisByPaisEmisionId() {
        return paisByPaisEmisionId;
    }

    public void setPaisByPaisEmisionId(Pais paisByPaisEmisionId) {
        this.paisByPaisEmisionId = paisByPaisEmisionId;
    }
}
